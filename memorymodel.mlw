module VAL_TYPES

  use set.Fset
  use int.Int

  type value 'a = fset 'a
  val function vundef 'a : value 'a 
  
  function compat (memtype1: fset 'a) (memtype2: fset 'a) : bool
  function sizeof (memtype: fset 'a) : int
  function alignof (memtype: fset 'a) : int
  function convert (memtype: fset 'a) (v: value 'a) : value 'a
  
  axiom sizeof_pos:
    forall ty: fset 'a. sizeof ty > 0
    
  axiom compat_sizeof:
    forall ty ty': fset 'a. compat ty ty' -> sizeof ty' = sizeof ty
    
    
  axiom alignof_pos:
    forall ty: fset 'a. alignof ty > 0
 
  val function max_alignment : int
  
  predicate divide (x: int) (y: int)
  = exists z. y = z*x
  
  axiom alignof_div: forall ty: fset 'a.
    (divide (alignof ty) max_alignment) 
  
  axiom compat_alignof:
    forall ty ty':fset 'a. compat ty ty' -> alignof ty' = alignof ty

end

module GEM_MEM
 
  use export set.Fset
  use export int.Int
  use export option.Option
  use export VAL_TYPES
 
  type mem 'a = fset 'a
  type block 'a = fset 'a
 
  function alloc (m: mem 'a) (l: int) (h: int) : option (block 'a, mem 'a)  
  function load (memtype: fset 'a) (m: mem 'a) (b: block 'a) (i: int) : option (fset 'a)
  function store (memtype: fset 'a) (m: mem 'a) (b: block 'a) (i: int) (v: fset 'a) : option (mem 'a)
  function free (m: mem 'a) (b: block 'a) : option (mem 'a)
 
  axiom load_alloc_other:
    forall m: fset 'a, lo hi b m' ty b' ofs.
      alloc m lo hi = Some (b,m') ->
      b' <> b ->
      load ty m' b' ofs = load ty m b' ofs
    
  axiom load_store_same:
    forall ty, m: fset 'a, b ofs v m' ty'.
      store ty m b ofs v = Some m' ->
      compat ty ty' ->
      load ty' m' b ofs = Some (convert ty' v)
      
      
  axiom load_store_disjoin:
    forall ty m b ofs v, m':fset 'a ,ty' b' ofs'.
      store ty m b ofs v = Some m' ->
      b' <> b \/ ofs' + sizeof ty' <= ofs \/ ofs + sizeof ty <= ofs' ->
      load ty' m' b' ofs' = load ty' m b' ofs'
      
  axiom load_free_other:
    forall m:fset 'a, b m' ty b' ofs.
      free m b = Some m' ->
      b' <> b ->
      load ty m' b' ofs = load ty m b' ofs
      
      
  function valid_block (mem: mem 'a) (b: block 'a) : bool
  
  axiom alloc_balid_block:
    forall m:fset 'a, lo hi b m' b'.
      alloc m lo hi = Some (b, m') ->
      (valid_block m' b' <-> b' = b \/ valid_block m b')
      
  axiom alloc_not_valid_block:
    forall m: fset 'a, lo hi b m'.
      alloc m lo hi = Some (b, m') ->
        not valid_block m b

  axiom load_valid_block:
    forall ty, m: fset 'a, b ofs v.
      load ty m b ofs = Some v ->
      valid_block m b

  axiom store_valid_block:
    forall ty, m: fset 'a, b ofs v m'.
      store ty m b ofs v = Some m' ->
      valid_block m b
  
  axiom store_valid_block_inv:
    forall ty, m: fset 'a, b ofs v m' b'.
      store ty m b ofs v = Some m' -> 
      (valid_block m' b' <-> valid_block m b')
        
  axiom free_valid_block:
    forall m: fset 'a, b m' b'.
      free m b = Some m' -> 
      b' <> b -> 
      (valid_block m' b' <-> valid_block m b')
      
  axiom valid_block_free:
    forall m: fset 'a, b.
      valid_block m b -> 
      exists m'. free m b = Some m'
      

  function bounds (m: mem 'a) (b: block 'a) : (int, int)       
  
  axiom alloc_result_bounds:
    forall m: fset 'a, lo hi b m'.
      alloc m lo hi = Some(b, m') -> 
      bounds m' b = (lo, hi)
      
  axiom alloc_bounds_inv:
    forall m: fset 'a, lo hi b m' b'.
      alloc m lo hi = Some(b, m') -> 
      b' <> b -> 
      bounds m' b' = bounds m b'
  
  axiom store_bounds_inv:
    forall ty, m: fset 'a, b ofs v m' b'.
      store ty m b ofs v = Some m' -> 
      bounds m' b' = bounds m b'
  
  axiom free_bounds_inv:
    forall m: fset 'a, b m' b'.
      free m b = Some m' -> 
      b' <> b -> 
      bounds m' b' = bounds m b'
      
  function fst (p: ('a,'b)) : 'a
  = let x, _ = p in x
  
  
  function snd (p: ('a,'b)) : 'b
  = let _, y = p in y
  
  predicate valid_pointer (ty: fset 'a) (m: mem 'a) (b: block 'a) (ofs: int) =
    valid_block m b /\ fst (bounds m b) <= ofs /\ 
    (divide (alignof ty) ofs) /\ 
    ofs + (sizeof ty) <= snd (bounds m b)
    

  axiom valid_pointer_store:
    forall ty, m: fset 'a, b ofs v.
      valid_pointer ty m b ofs ->
      exists m'. store ty m b ofs v = Some m'


  
end


module Gen_Mem_Facts

  use export GEM_MEM
  
  lemma alloc_valid_block_inv:
    forall m:fset 'a, lo hi b m' b'.
      alloc m lo hi = Some(b, m') -> valid_block m b' -> valid_block m' b'
      
  lemma alloc_not_valid_block_2:
    forall m:fset 'a, lo hi b m' b'.
      alloc m lo hi = Some(b, m') -> 
      valid_block m b' -> b' <> b
  
  
  lemma load_alloc_other_2:
    forall m:fset 'a, lo hi b m' b' ty ofs v.
      alloc m lo hi = Some(b, m') ->
      load ty m b' ofs = Some v ->
      load ty m' b' ofs = Some v
  
  
  lemma alloc_result_valid_pointer:
    forall m:fset 'a, lo hi b m' ty ofs.
      alloc m lo hi = Some(b, m') ->
      (divide (alignof ty) ofs) -> lo <= ofs -> ofs + sizeof ty <= hi ->
      valid_pointer ty m' b ofs
  
  
  lemma alloc_valid_pointer_inv:
    forall m:fset 'a, lo hi b m' ty b' ofs.
      alloc m lo hi = Some(b, m') ->
      valid_pointer ty m b' ofs -> valid_pointer ty m' b' ofs
  
  
  lemma store_valid_pointer_inv:
    forall ty, m:fset 'a, b ofs v m' ty' b' ofs'.
      store ty m b ofs v = Some m' ->
      (valid_pointer ty' m' b' ofs' <-> valid_pointer ty' m b' ofs')
  
  lemma free_valid_pointer_inv:
    forall m:fset 'a, b m' ty b' ofs.
      free m b = Some m' -> b' <> b ->
      (valid_pointer ty m' b' ofs <-> valid_pointer ty m b' ofs)
      
      
  function proj_pointer (v: value 'a) : option (block 'a, int)

  function loadv (ty: fset 'a) (m: mem 'a) (v: value 'a) : option (value 'a)
  = match proj_pointer v with
    | Some(b, ofs) -> load ty m b ofs
    | None -> None
    end

  function storev (ty: fset 'a) (m: mem 'a) (addr v: value 'a) : option (mem 'a)
  = match proj_pointer addr with
    | Some(b, ofs) -> store ty m b ofs v
    | None -> None
    end

end

module REF_GEN_MEM

  use export Gen_Mem_Facts
  
  axiom valid_pointer_load:
    forall ty, m: mem 'a, b ofs.
      valid_pointer ty m b ofs ->
      exists v. load ty m b ofs = Some v
  
  axiom store_valid_pointer:
    forall ty, m: mem 'a, b ofs v m'.
      store ty m b ofs v = Some m' -> 
      valid_pointer ty m b ofs
  
  axiom load_valid_pointer:
    forall ty, m: mem 'a, b ofs v.
      load ty m b ofs = Some v -> 
      valid_pointer ty m b ofs
      
  axiom load_alloc_same:
    forall m: mem 'a, lo hi b m' ty ofs, v: value 'a.
      alloc m lo hi = Some (b, m') ->
      load ty m' b ofs = Some v ->
      exists a. v = (vundef a)
      
  axiom load_store_mismatch:
    forall ty, m: mem 'a, b ofs v m' ty' v'.
      store ty m b ofs v = Some m' ->
      not compat ty ty' ->
      load ty' m' b ofs = Some v' ->
      exists a. v' = (vundef a)
      
      
  axiom load_store_overlap:
    forall ty, m: mem 'a, b ofs v m' ty' ofs' v'.
      store ty m b ofs v = Some m' ->
      ofs' <> ofs -> ofs' + sizeof ty' > ofs -> ofs + sizeof ty > ofs' ->
      load ty' m' b ofs' = Some v' ->
      exists a. v' = (vundef a)
      
  function fresh_block (m: mem 'a) (b: block 'a) : bool
  
  axiom fresh_valid_block_exclusive:
    forall m: mem 'a, b.
      fresh_block m b -> valid_block m b -> False
      
  axiom alloc_fresh_block:
    forall m: mem 'a, lo hi b m' b'.
      alloc m lo hi = Some(b, m') ->
      (fresh_block m' b' <-> b <> b' /\ fresh_block m b')
      
  axiom alloc_fresh_block_2:
    forall m: mem 'a, lo hi b m'.
      alloc m lo hi = Some(b, m') -> fresh_block m b
      
  axiom store_fresh_block:
    forall m: mem 'a, chunk b ofs v m' b'.
      store chunk m b ofs v = Some m' ->
      (fresh_block m' b' <-> fresh_block m b')
      
  axiom free_fresh_block:
    forall m: mem 'a, b m' b'.
      free m b = Some m' ->
      (fresh_block m' b' <-> fresh_block m b')
      
  predicate same_domain (m1 m2: mem 'a) 
  = forall b. fresh_block m1 b <-> fresh_block m2 b
  
  
  axiom alloc_same_domain:
    forall m1: mem 'a, lo1 hi1 b1 m1' m2 lo2 hi2 b2 m2'.
      alloc m1 lo1 hi1 = Some(b1, m1') ->
      alloc m2 lo2 hi2 = Some(b2, m2') ->
      same_domain m1 m2 ->
      b1 = b2 /\ same_domain m1' m2'
      
  axiom free_not_valid_block:
    forall m: mem 'a, b m'.
      free m b = Some m' -> not (valid_block m' b)
      
  axiom free_same_bounds:
    forall m: mem 'a, b m'.
      free m b = Some m' ->
      fst(bounds m' b) = snd(bounds m' b)
      
end

module Ref_Gen_Mem_Facts

  use export REF_GEN_MEM
  
  lemma store_valid_pointer_2:
    forall ty, m: mem 'a, b ofs v m'.
      store ty m b ofs v = Some m' -> valid_pointer ty m' b ofs
      
  lemma load_alloc_same_2:
    forall m: mem 'a, lo hi b m' ty ofs.
      alloc m lo hi = Some (b, m') ->
      (divide (alignof ty) ofs) -> lo <= ofs -> ofs + sizeof ty <= hi ->
      exists a. load ty m' b ofs = Some (vundef a)
      
  lemma load_store_mismatch_2:
    forall ty, m:mem 'a, b ofs v m' ty'.
      store ty m b ofs v = Some m' ->
      not (compat ty ty') ->
      valid_pointer ty' m b ofs ->
      exists a. load ty' m' b ofs = Some (vundef a)
      
  lemma load_store_overlap_2:
    forall ty, m: mem 'a ,b ofs v m' ty' ofs'.
      store ty m b ofs v = Some m' ->
      ofs' <> ofs -> ofs' + sizeof ty' > ofs -> ofs + sizeof ty > ofs' ->
      valid_pointer ty' m b ofs' ->
      exists a. load ty' m' b ofs' = Some (vundef a)
      
      
  inductive load_store_cases (ty1: fset 'a) (b1: block 'a) (ofs1: int) (ty2: fset 'a) (b2: block 'a) (ofs2: int) =
    | lsc_similar: forall b1: block 'a, b2 ofs1 ofs2 ty1 ty2.
        b1 = b2 -> ofs1 = ofs2 -> compat ty1 ty2 ->
        load_store_cases ty1 b1 ofs1 ty2 b2 ofs2
    | lsc_other: forall b1: block 'a, b2 ofs2 ty2 ofs1 ty1.
        b1 <> b2 \/ ofs2 + sizeof ty2 <= ofs1 \/ ofs1 + sizeof ty1 <= ofs2 ->
        load_store_cases ty1 b1 ofs1 ty2 b2 ofs2
    | lsc_overlap: forall b1: block 'a, b2, ofs1, ofs2, ty2, ty1.
        b1 = b2 -> ofs1 <> ofs2 -> ofs2 + sizeof ty2 > ofs1 -> ofs1 + sizeof ty1 > ofs2 ->
        load_store_cases ty1 b1 ofs1 ty2 b2 ofs2
    | lsc_mismatch: forall b1: block 'a, b2 ofs1 ofs2 ty1 ty2.
      b1 = b2 -> ofs1 = ofs2 -> not compat ty1 ty2 ->
      load_store_cases ty1 b1 ofs1 ty2 b2 ofs2
      
  lemma load_store_classification:
    forall ty1: fset 'a, b1 ofs1 ty2 b2 ofs2.
      load_store_cases ty1 b1 ofs1 ty2 b2 ofs2  
  
  lemma load_store_characterization_lsc_similar:
    forall ty: fset 'a, m1 b ofs v m2 ty' b' ofs'.
      store ty m1 b ofs v = Some m2 ->
      valid_pointer ty' m1 b' ofs' ->
        load_store_cases ty b ofs ty' b' ofs' /\ b = b' /\ ofs = ofs' /\ compat ty ty' ->
        load ty' m2 b' ofs' = Some (convert ty' v)
        
  lemma load_store_characterization_lsc_other:
    forall ty: fset 'a, m1 b ofs v m2 ty' b' ofs'.
      store ty m1 b ofs v = Some m2 ->
      valid_pointer ty' m1 b' ofs' ->
        load_store_cases ty b ofs ty' b' ofs' /\ (b <> b' \/ ofs' + sizeof ty' <= ofs \/ ofs + sizeof ty <= ofs') ->
        load ty' m2 b' ofs' = load ty' m1 b' ofs'
        
  lemma load_store_characterization_lsc_overlap:
    forall ty: fset 'a, m1 b ofs v m2 ty' b' ofs'.
      store ty m1 b ofs v = Some m2 ->
      valid_pointer ty' m1 b' ofs' ->
        load_store_cases ty b ofs ty' b' ofs' /\ b = b' /\ ofs <> ofs' /\ ofs' + sizeof ty' > ofs /\ ofs + sizeof ty > ofs' ->
        exists a. load ty' m2 b' ofs' = Some (vundef a)
        
  lemma load_store_characterization_lsc_mismatch:
    forall ty: fset 'a, m1 b ofs v m2 ty' b' ofs'.
      store ty m1 b ofs v = Some m2 ->
      valid_pointer ty' m1 b' ofs' ->
        load_store_cases ty b ofs ty' b' ofs' /\ b = b' /\ ofs = ofs' /\ not compat ty ty' ->
        exists a. load ty' m2 b' ofs' = Some (vundef a)
        
        
  lemma store_same_domain:
    forall ty1:fset 'a, m1 b1 ofs1 v1 m1' ty2 m2 b2 ofs2 v2 m2'.
      store ty1 m1 b1 ofs1 v1 = Some m1' ->
      store ty2 m2 b2 ofs2 v2 = Some m2' ->
      same_domain m1 m2 ->
      same_domain m1' m2'
      
  lemma free_same_domain:
    forall m1: mem 'a, b m1' m2 m2'.
      free m1 b = Some m1' ->
      free m2 b = Some m2' ->
      same_domain m1 m2 ->
      same_domain m1' m2'
      
  lemma free_not_valid_pointer:
    forall m: mem 'a, b m'.
      free m b = Some m' ->
      forall ty ofs. not (valid_pointer ty m' b ofs)
  
        
end

module Concrete_Mem

  use export Ref_Gen_Mem_Facts
  use int.EuclideanDivision

  function update (x: int) (v: 'a) (f: int -> 'a): int -> 'a =
    fun y -> if y = x then v else f y
    
  lemma update_s:
    forall x, v: fset 'a, f.
      update x v f x = v
      
  lemma update_o:
    forall x, v: fset 'a, f y.
      y <> x -> update x v f y = f y
      
  type content 'a = option (fset 'a, value 'a)
  
  type block_ = int
  
  type mem_ 'a = {
    nextblock: int;
    bounds_: block_ -> (int,int);
    freed: block_ -> bool;
    contents: block_ -> int -> content 'a
  }
  
  
  val empty_mem () : mem_ 'a
    ensures { result.nextblock = 1 }
    ensures { result.bounds_ = fun b -> (0,0) }
    ensures { result.freed = fun b -> false }
    ensures { result.contents = fun b ofs -> None }
    
    
  predicate fresh_block_ (m: mem_ 'a) (b: block_) =
    b >= nextblock m
    
  predicate valid_block_ (m: mem_ 'a) (b: block_) =
    b < nextblock m /\ freed m b = false
    
  lemma valid_block_dec:
    forall m: mem 'a, b. valid_block m b \/ not valid_block m b
    
  predicate valid_pointer_ (ty: fset 'a) (m: mem_ 'a) (b: block_) (ofs: int) =
    valid_block_ m b
    /\ (divide (alignof ty) ofs)
    /\ fst (bounds_ m b) <= ofs
    /\ ofs + sizeof ty <= snd (bounds_ m b)
    
    
  lemma zdivide_Zmod:
    forall a b. b > 0 -> ((divide b a) <-> mod a b = 0)
    
  lemma aligned_dec:
    forall ty: fset 'a, ofs. (divide (alignof ty) ofs) \/ not (divide (alignof ty) ofs)
    
  lemma valid_pointer_dec:
    forall ty: fset 'a, m b ofs. valid_pointer_ ty m b ofs \/ not valid_pointer_ ty m b ofs
    
    
  let rec function check_cont (f: int -> content 'a) (ofs: int) (n: int) : bool
    requires { n >= 0 }
    variant { n }
  = if n = 0 then true else
  match f ofs with
    | None -> check_cont f (ofs + 1) (n-1)
    | _ -> false
  end
  
  lemma check_cont_charact:
    forall f: (int -> option (fset 'a, fset 'a)) , n ofs.
      n >= 0 ->
      if check_cont f ofs n
      then (forall i. ofs <= i < ofs + n -> f i = None)
      else (exists i. ofs <= i < ofs + n /\ f i <> None)
      
  lemma check_cont_exten:
    forall f1: (int -> option (fset 'a, fset 'a)), f2 n ofs.
      n >= 0 ->
      (forall i. ofs <= i < ofs + n -> f2 i = f1 i) ->
      check_cont f2 ofs n = check_cont f1 ofs n
      
  function load_contents (a: 'a) (ty: fset 'a) (f: int -> content 'a) (ofs: int) : value 'a
  = match f ofs with
    | Some(ty', v) ->
        if compat ty' ty then
          if check_cont f (ofs + 1) (sizeof ty - 1)
          then convert ty v
          else vundef a
        else vundef a
    | _ -> vundef a
    end
  
  
  lemma load_contents_exten:
    forall a, f1:(int -> content 'a), f2: (int -> content 'a), ty: fset 'a, ofs.
      (forall i. ofs <= i < ofs + sizeof ty -> f2 i = f1 i) ->
      load_contents a ty f2 ofs = load_contents a ty f1 ofs
      
  lemma load_contents_1:
    forall a:'a, ty f ofs ty' v.
      f ofs = Some(ty', v) ->
      compat ty' ty ->
      (forall i. ofs + 1 <= i < ofs + sizeof ty -> f i = None) ->
      load_contents a ty f ofs = convert ty v
      
  lemma load_contents_2:
    forall a: 'a, ty f ofs.
      f ofs = None ->
      load_contents a ty f ofs = vundef a
      
  lemma load_contents_3:
    forall a: 'a, ty f ofs ty' v.
      f ofs = Some(ty', v) ->
      not compat ty' ty ->
      load_contents a ty f ofs = vundef a
    
  lemma load_contents_4:
    forall a: 'a, ty f ofs i.
      ofs + 1 <= i < ofs + sizeof ty -> f i <> None ->
      load_contents a ty f ofs = vundef a
      
  let rec ghost function set_cont (f: int -> content 'a) (ofs: int) (n: int) : int -> content 'a
    requires { n >= 0 }
    variant { n }
  = if n = 0 then f else
    set_cont (update ofs None f) (ofs + 1) (n-1)
    
    
  lemma set_cont_outside:
    forall n, f:(int -> option (fset 'a, fset 'a)) , ofs i.
      n >= 0 ->
      i < ofs \/ i >= ofs + n -> set_cont f ofs n i = f i
  
  
  lemma set_cont_inside:
    forall n , f:(int -> option (fset 'a, fset 'a)) , ofs i.
      ofs <= i < ofs + n -> set_cont f ofs n i = None
  
  function store_contents (f: int -> content 'a) (ty: fset 'a) (ofs: int) (v: value 'a): int -> content 'a =
    set_cont (update ofs (Some (ty, v)) f) (ofs + 1) (sizeof ty - 1)
  
  lemma store_contents_at:
    forall f:(int -> option (fset 'a, fset 'a)) , ty ofs v.
      store_contents f ty ofs v ofs = Some (ty, v)
      
  lemma store_contents_cont:
    forall f:(int -> option (fset 'a, fset 'a)), ty ofs v i.
      ofs < i < ofs + sizeof ty ->
      store_contents f ty ofs v i = None
  
  lemma store_contents_outside:
    forall f:(int -> option (fset 'a, fset 'a)), ty ofs v i.
      i < ofs \/ i >= ofs + sizeof ty ->
      store_contents f ty ofs v i = f i
      
  lemma load_store_contents_same:
    forall f:(int -> option (fset 'a, fset 'a)), ty ty' ofs v a.
      compat ty ty' ->
      load_contents a ty' (store_contents f ty ofs v) ofs = convert ty' v
      
  lemma load_store_contents_disjoint:
    forall a: 'a, f ty ty' ofs ofs' v.
      ofs' + sizeof ty' <= ofs \/ ofs + sizeof ty <= ofs' ->
      load_contents a ty' (store_contents f ty ofs v) ofs' =
      load_contents a ty' f ofs'
      
  lemma load_store_contents_mismatch:
    forall a:'a, f ty ty' ofs v.
      not compat ty ty' ->
      load_contents a ty' (store_contents f ty ofs v) ofs = vundef a
      
  lemma load_store_contents_overlap:
    forall a:'a, f ty ty' ofs ofs' v.
      ofs' <> ofs -> ofs' + sizeof ty' > ofs -> ofs + sizeof ty > ofs' ->
      load_contents a ty' (store_contents f ty ofs v) ofs' = vundef a
      
      
  function enough_free_memory (mem: mem_ 'a) (i: int) : bool
   
  function alloc_ (m: mem_ 'a) (lo hi: int) : option (block_ , mem_ 'a) =
    if enough_free_memory m (hi - lo)
    then Some(nextblock m,
               { nextblock = (nextblock m + 1);
                 bounds_ = ( update (nextblock m) (lo, hi) (bounds_ m));
                freed = (update (nextblock m) false (freed m));
                contents = (update (nextblock m) (fun (ofs: int) -> None) (contents m)) } )
    else None
    
    
  function load_ (a: 'a) (ty: fset 'a) (m: mem_ 'a) (b: block_) (ofs: int) : option (value 'a) =
    if valid_pointer_ ty m b ofs
    then Some (load_contents a ty (contents m b) ofs)
    else None

  function store_ (ty: fset 'a) (m: mem_ 'a) (b: block_) (ofs: int) (v: value 'a) : option (mem_ 'a) =
    if valid_pointer_ ty m b ofs
    then Some( { nextblock = (nextblock m);
                 bounds_ = (bounds_ m);
                 freed = (freed m);
                 contents = (update b (store_contents (contents m b) ty ofs v) (contents m))
                })
    else None
    
  function free_ (m: mem_ 'a) (b: block_) : option (mem_ 'a) =
    if freed m b
    then None
    else Some( { nextblock = (nextblock m);
                 bounds_ = (update b (0,0) (bounds_ m));
                 freed = (update b true (freed m));
                 contents = (contents m)
                })
                
  lemma alloc_result_bounds_:
    forall m: mem_ 'a, lo hi b m'.
      alloc_ m lo hi = Some(b, m') -> bounds_ m' b = (lo, hi)
      
  lemma alloc_bounds_inv_:
    forall m: mem_ 'a, lo hi b m' b'.
      alloc_ m lo hi = Some(b, m') -> b' <> b -> bounds_ m' b' = bounds_ m b'
      
  lemma store_bounds_inv_:
    forall ty, m: mem_ 'a, b ofs v m' b'.
      store_ ty m b ofs v = Some m' -> bounds_ m' b' = bounds_ m b'
      
  lemma free_bounds_inv_:
    forall m: mem_ 'a, b m' b'.
      free_ m b = Some m' -> b' <> b -> bounds_ m' b' = bounds_ m b'
      
  lemma fresh_valid_block_exclusive_:
    forall m: mem_ 'a, b.
      fresh_block_ m b -> valid_block_ m b -> False
      
      
  lemma alloc_fresh_block_:
    forall m:mem_ 'a, lo hi b m' b'.
      alloc_ m lo hi = Some(b, m') ->
      (fresh_block_ m' b' <-> b <> b' /\ fresh_block_ m b')
      
  lemma alloc_fresh_block_2_:
    forall m:mem_ 'a, lo hi b m'.
      alloc_ m lo hi = Some(b, m') -> fresh_block_ m b
      
  lemma store_inversion:
    forall ty: fset 'a, m b ofs v m'.
      store_ ty m b ofs v = Some m' ->
      valid_pointer_ ty m b ofs /\
      m' = { nextblock = (nextblock m);
             bounds_ = (bounds_ m);
             freed = (freed m);
             contents = (update b (store_contents (contents m b) ty ofs v) (contents m))
           }       
           
  lemma store_fresh_block_:
    forall m: mem_ 'a, chunk b ofs v m' b'.
      store_ chunk m b ofs v = Some m' ->
      (fresh_block_ m' b' <-> fresh_block_ m b')
      
  lemma free_fresh_block_:
    forall m: mem_ 'a, b m' b'.
      free_ m b = Some m' ->
      (fresh_block_ m' b' <-> fresh_block_ m b')
      
  lemma alloc_valid_block:
    forall m: mem_ 'a, lo hi b m' b'.
      alloc_ m lo hi = Some(b, m') ->
      (valid_block_ m' b' <-> b' = b \/ valid_block_ m b')
      
  lemma alloc_not_valid_block_:
    forall m: mem_ 'a, lo hi b m'.
    alloc_ m lo hi = Some(b, m') -> not (valid_block_ m b)

  lemma load_valid_block_:
    forall ty:fset 'a, a m b ofs v.
    load_ a ty m b ofs = Some v -> valid_block_ m b

  lemma store_valid_block_:
    forall ty: fset 'a, m b ofs v m'.
    store_ ty m b ofs v = Some m' -> valid_block_ m b

  lemma store_valid_block_inv_:
    forall ty: fset 'a, m b ofs v m' b'.
    store_ ty m b ofs v = Some m' -> (valid_block_ m' b' <-> valid_block_ m b')

  lemma free_valid_block_:
    forall m: mem_ 'a, b m' b'.
    free_ m b = Some m' -> b' <> b -> (valid_block_ m' b' <-> valid_block_ m b')

  lemma store_valid_pointer_inv_:
    forall ty: fset 'a, m b ofs v m' ty' b' ofs'.
    store_ ty m b ofs v = Some m' ->
    (valid_pointer_ ty' m' b' ofs' <-> valid_pointer_ ty' m b' ofs')

  lemma alloc_valid_pointer_inv_:
    forall m: mem_ 'a, lo hi b m' ty b' ofs.
    alloc_ m lo hi = Some(b, m') -> b' <> b ->
    (valid_pointer_ ty m b' ofs <-> valid_pointer_ ty m' b' ofs)

  lemma free_valid_pointer_inv_:
    forall m: mem_ 'a, b m' ty b' ofs.
    free_ m b = Some m' -> b' <> b ->
    (valid_pointer_ ty m' b' ofs <-> valid_pointer_ ty m b' ofs)

  lemma load_alloc_other_:
    forall m: mem_ 'a, a lo hi b m' ty b' ofs.
    alloc_ m lo hi = Some (b, m') ->
    b' <> b ->
    load_ a ty m' b' ofs = load_ a ty m b' ofs

  lemma valid_pointer_compat_:
    forall ty, m: mem_ 'a, b ofs ty'.
    compat ty ty' ->
    (valid_pointer_ ty m b ofs <-> valid_pointer_ ty' m b ofs)

  lemma load_store_same_:
    forall ty:fset 'a, m: mem_ 'a, a b ofs v m' ty'.
    store_ ty m b ofs v = Some m' ->
    compat ty ty' ->
    load_ a ty' m' b ofs = Some (convert ty' v)

  lemma load_store_disjoint_:
    forall ty, m: mem_ 'a, a b ofs v m' ty' b' ofs'.
    store_ ty m b ofs v = Some m' ->
    b' <> b \/ ofs' + sizeof ty' <= ofs \/ ofs + sizeof ty <= ofs' ->
    load_ a ty' m' b' ofs' = load_ a ty' m b' ofs'

  lemma load_free_other_:
    forall m: mem_ 'a, a b m' ty b' ofs.
    free_ m b = Some m' ->
    b' <> b ->
    load_ a ty m' b' ofs = load_ a ty m b' ofs

  lemma load_alloc_same_:
    forall a: 'a, m lo hi b m' ty ofs v.
    alloc_ m lo hi = Some (b, m') ->
    load_ a ty m' b ofs = Some v ->
    v = (vundef a)

  lemma load_store_mismatch_:
    forall a: 'a,ty m b ofs v m' ty' v'.
    store_ ty m b ofs v = Some m' ->
    not (compat ty ty') ->
    load_ a ty' m' b ofs = Some v' ->
    v' = (vundef a)

  lemma load_store_overlap_:
    forall a: 'a, ty m b ofs v m' ty' ofs' v'.
    store_ ty m b ofs v = Some m' ->
    ofs' <> ofs -> ofs' + sizeof ty' > ofs -> ofs + sizeof ty > ofs' ->
    load_ a ty' m' b ofs' = Some v' ->
    v' = (vundef a)
    
  predicate same_domain_ (m1 m2: mem_ 'a) =
    forall b. fresh_block_ m1 b <-> fresh_block_ m2 b

  lemma same_domain_same_nextblock:
    forall m1: mem_ 'a, m2. same_domain_ m1 m2 -> nextblock m2 = nextblock m1

  lemma alloc_same_domain_:
    forall m1: mem_ 'a, lo1 hi1 b1 m1' m2 lo2 hi2 b2 m2'.
    alloc_ m1 lo1 hi1 = Some(b1, m1') ->
    alloc_ m2 lo2 hi2 = Some(b2, m2') ->
    same_domain_ m1 m2 ->
    b1 = b2 /\ same_domain_ m1' m2'

  lemma valid_block_free_:
    forall m: mem_ 'a, b.
    valid_block_ m b -> exists m'. free_ m b = Some m'

  lemma valid_pointer_store_:
    forall ty, m: mem_ 'a, b ofs v.
    valid_pointer_ ty m b ofs ->
    exists m'. store_ ty m b ofs v = Some m'

  lemma store_valid_pointer_:
    forall ty, m: mem_ 'a, b ofs v m'.
    store_ ty m b ofs v = Some m' -> valid_pointer_ ty m b ofs

  lemma valid_pointer_load_:
    forall ty, m: mem_ 'a, a b ofs.
    valid_pointer_ ty m b ofs ->
    exists v. load_ a ty m b ofs = Some v

  lemma load_valid_pointer_:
    forall a: 'a,ty m b ofs v.
    load_ a ty m b ofs = Some v -> valid_pointer_ ty m b ofs

  lemma free_not_valid_block_:
    forall m:mem_ 'a, b m'.
    free_ m b = Some m' ->
    not (valid_block_ m' b)

  lemma free_same_bounds_:
    forall m: mem_ 'a, b m'.
    free_ m b = Some m' ->
    fst(bounds_ m' b) = snd(bounds_ m' b)
  
end

module Rel_Mem

  use export Concrete_Mem
  use list.List
  use list.Mem
  
  type embedding = (block_ -> option (block_, int))

  function val_emb (e: embedding) (v1: value 'a) (v2: value 'a) : bool
  
  predicate mem_emb (emb: embedding) (m1 m2: mem_ 'a) =
    forall a: 'a, ty b1 ofs v1 b2 delta.
      emb b1 = Some(b2, delta) ->
      load_ a ty m1 b1 ofs = Some v1 ->
      exists v2. load_ a ty m2 b2 (ofs + delta) = Some v2 /\ val_emb emb v1 v2
      
  lemma valid_pointer_emb:
    forall emb, m1: mem_ 'a, m2 ty b1 ofs b2 delta.
      emb b1 = Some(b2, delta) ->
      mem_emb emb m1 m2 ->
      valid_pointer_ ty m1 b1 ofs ->
      valid_pointer_ ty m2 b2 (ofs + delta)
      
  lemma store_unmapped_emb:
    forall emb, m1: mem_ 'a, m2 b ofs v ty m1'.
      mem_emb emb m1 m2 ->
      emb b = None ->
      store_ ty m1 b ofs v = Some m1' ->
      mem_emb emb m1' m2
      
      
  lemma store_outside_emb:
  forall emb, m1: mem_ 'a, m2 ty b ofs v m2'.
  mem_emb emb m1 m2 ->
  (forall b' delta.
     emb b' = Some(b, delta) ->
     snd(bounds_ m1 b') + delta <= ofs \/ ofs + sizeof ty <= fst(bounds_ m1 b') + delta) ->
  store_ ty m2 b ofs v = Some m2' ->
  mem_emb emb m1 m2'
  
  predicate embedding_no_overlap (emb: embedding) (m: mem_ 'a) =
    forall b1 b1' delta1 b2 b2' delta2.
    b1 <> b2 ->
    emb b1 = Some (b1', delta1) ->
    emb b2 = Some (b2', delta2) ->
    b1' <> b2'
    \/ fst(bounds_ m b1) >= snd(bounds_ m b1)
    \/ fst(bounds_ m b2) >= snd(bounds_ m b2)
    \/ snd(bounds_ m b1) + delta1 <= fst(bounds_ m b2) + delta2
    \/ snd(bounds_ m b2) + delta2 <= fst(bounds_ m b1) + delta1
  
  lemma store_mapped_emb:
    forall a: 'a, emb, m1: mem_ 'a, m2 b1 ofs b2 delta v1 v2 ty m1'.
      mem_emb emb m1 m2 ->
      embedding_no_overlap emb m1 ->
      val_emb emb (vundef a) (vundef a) ->
      emb b1 = Some(b2, delta) ->
      store_ ty m1 b1 ofs v1 = Some m1' ->
      (forall ty'. compat ty ty' ->
        val_emb emb (convert ty' v1) (convert ty' v2)) ->
      exists m2'.
      store_ ty m2 b2 (ofs + delta) v2 = Some m2' /\ mem_emb emb m1' m2'
      
      
  lemma alignment_shift:
    forall ty: fset 'a, ofs delta.
      (divide (alignof ty) ofs) -> (divide (max_alignment) delta) ->
      (divide (alignof ty) (ofs + delta))
      
  lemma alloc_parallel_emb:
    forall a: 'a, emb, m1 : mem_ 'a, m2 lo1 hi1 m1' b1 lo2 hi2 m2' b2 delta.
      mem_emb emb m1 m2 ->
      alloc_ m1 lo1 hi1 = Some(b1, m1') ->
      alloc_ m2 lo2 hi2 = Some(b2, m2') ->
      emb b1 = Some(b2, delta) ->
      lo2 <= lo1 + delta -> hi1 + delta <= hi2 ->
      (divide max_alignment delta) ->
      val_emb emb (vundef a) (vundef a) ->
      mem_emb emb m1' m2'
      
  lemma alloc_right_emb:
    forall emb, m1: mem_ 'a, m2 lo hi b2 m2'.
      mem_emb emb m1 m2 ->
      alloc_ m2 lo hi = Some(b2, m2') ->
      mem_emb emb m1 m2'
      
  lemma alloc_left_unmapped_emb:
    forall emb, m1 : mem_ 'a, m2 lo hi b1 m1'.
      mem_emb emb m1 m2 ->
      alloc_ m1 lo hi = Some(b1, m1') ->
      emb b1 = None ->
      mem_emb emb m1' m2
      
  lemma alloc_left_mapped_emb:
    forall a: 'a, emb, m1 : mem_ 'a, m2 lo hi b1 m1' b2 delta.
      mem_emb emb m1 m2 ->
      alloc_ m1 lo hi = Some(b1, m1') ->
      emb b1 = Some(b2, delta) ->
      valid_block_ m2 b2 ->
      fst(bounds_ m2 b2) <= lo + delta -> hi + delta <= snd(bounds_ m2 b2) ->
      (divide max_alignment delta) ->
      (forall v. val_emb emb (vundef a) v) ->
      mem_emb emb m1' m2
      
  lemma free_left_emb:
    forall emb, m1: mem_ 'a, m2 b1 m1'.
      mem_emb emb m1 m2 ->
      free_ m1 b1 = Some m1' ->
      mem_emb emb m1' m2
      
  lemma free_right_emb:
    forall emb, m1 : mem_ 'a, m2 b2 m2'.
      mem_emb emb m1 m2 ->
      (forall b1 delta.
       emb b1 = Some(b2, delta) -> not (valid_block_ m1 b1)) ->
      free_ m2 b2 = Some m2' ->
      mem_emb emb m1 m2'
  
  function free_list (m: mem_ 'a) (bl: list block_) : option (mem_ 'a) =
    match bl with
    | nil -> Some m
    | Cons b bs ->
        match free_list m bs with
        | None -> None
        | Some m1 -> free_ m1 b
        end
    end
    
  lemma free_list_left_emb:
    forall emb, m2: mem_ 'a, l m1 m1'.
      mem_emb emb m1 m2 ->
      free_list m1 l = Some m1' ->
      mem_emb emb m1' m2
  
  lemma free_list_not_valid_block:
    forall m: mem_ 'a, bl m'.
      free_list m bl = Some m' ->
      forall b. mem b bl -> not (valid_block_ m' b)
      
  lemma free_list_free_parallel_emb:
    forall emb, m1: mem_ 'a, m2 bl b2 m1' m2'.
      mem_emb emb m1 m2 ->
      free_list m1 bl = Some m1' ->
      free_ m2 b2 = Some m2' ->
      (forall b delta'. emb b = Some(b2, delta') -> mem b bl) ->
      mem_emb emb m1' m2'
      
  lemma free_parallel_emb:
    forall emb, m1 : mem_ 'a, m2 b1 b2 m1' m2'.
      mem_emb emb m1 m2 ->
      free_ m1 b1 = Some m1' ->
      free_ m2 b2 = Some m2' ->
      (forall b delta'. emb b = Some(b2, delta') -> b = b1) ->
      mem_emb emb m1' m2'
  
end